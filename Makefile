TARGET = reverse

all:
	gcc -Wall -Wextra -Werror *.c -o $(TARGET)

clean:
	rm -rf $(TARGET)  
