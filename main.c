#include "scan_array.h"
#include "print_array.h"
#include <stdio.h>
#include <stdlib.h>
#include "reverse.h"

int main()
{
	int n;
	printf("Enter n: ");
	scanf("%d", &n);
	int *arr = (int *) malloc(n * sizeof(int));
	scan_array(arr, n);
	reverse_arr(arr, n);
	print_array(arr, n);
	free(arr);
	return 0;
}
	
